<?php
namespace Omnipay\Jetonarme;

use Omnipay\Common\AbstractGateway;

/**
 * Jetonarme is Jeton Gateway Driver for Omnipay
 *
 * This driver is based on
 * Jeton Wallet API v3
 * @link https://developer.jeton.com/
 */
class Gateway extends AbstractGateway {

    public function getName()
    {
        return 'Jetonarme';
    }

    public function getDefaultParameters()
    {
        return [
            'apiKey'         => null,
            'testMode'       => false,
        ];
    }

    public function getApiKey()
    {
        return $this->getParameter('apiKey');
    }

    public function setApiKey($value)
    {
        return $this->setParameter('apiKey', $value);
    }

    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Jetonarme\Message\PurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Jetonarme\Message\CompletePurchaseRequest', $parameters);
    }

    public function fetchTransaction(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Jetonarme\Message\FetchTransactionRequest', $parameters);
    }

    public function refund(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Jetonarme\Message\RefundRequest', $parameters);
    }

}