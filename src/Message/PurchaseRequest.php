<?php

namespace Omnipay\Jetonarme\Message;


class PurchaseRequest extends AbstractRequest
{

    public function getFailRedirectUrl()
    {
        return $this->getParameter('failRedirectUrl');
    }

    public function setFailRedirectUrl($value)
    {
        $this->setParameter('failRedirectUrl', $value);
    }

    public function getCustomerNumber()
    {
        return $this->getParameter('customer');
    }

    public function getcustomerReferenceNo()
    {
        return $this->getParameter('customerReferenceNo');
    }


    public function setCustomerNumber($value)
    {
        $this->setParameter('customer', $value);
    }

    public function getData()
    {
        $this->validate('apiKey', 'transactionReference', 'amount', 'currency');

        $data                           = [];
        $data['orderId']                = $this->getTransactionReference();
        $data['amount']                 = $this->getAmount();
        $data['currency']               = $this->getCurrency();
        $data['method']                 = $this->getPaymentMethod();
        $data['customer']               = $this->getCustomerNumber();
        $data['customerReferenceNo']    = $this->getcustomerReferenceNo();


        if(!is_null($this->getReturnUrl())) {
            $data['returnUrl'] = $this->getReturnUrl();
        }

        if(!is_null($this->getCancelUrl())) {
            $data['cancelRedirectUrl'] = $this->getCancelUrl();
        }

        if(!is_null($this->getFailRedirectUrl())) {
            $data['failRedirectUrl'] = $this->getFailRedirectUrl();
        }

        if(!is_null($this->getItems())) {

            foreach($this->getItems() as $item) {
                $data['paymentItems'][] = [
                    'amount'      => $item->getPrice(),
                    'count'       => $item->getQuantity(),
                    'description' => $item->getDescription()
                ];
            }
        }

        return $data;
    }

    public function sendData($data)
    {
        $response = $this->sendRequest('POST', '/', $data);

        return $this->response = new PurchaseResponse($this, $response);
    }

    protected function sendRequest($method, $endpoint, $data = null)
    {
        $response = $this->httpClient->request(
            $method,
            $this->endpoint . $endpoint,
            [
                'X-API-KEY' => $this->getApiKey(),
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return json_decode($response->getBody()->getContents(), true);
    }
}